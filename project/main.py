#!/usr/bin/env python

from args import get_argparser
from services import DockerAwsLogs


def main():
    args = get_argparser()
    docker_aws_logs = DockerAwsLogs(args=args)
    docker_aws_logs.run_container()


if __name__ == '__main__':
    main()
