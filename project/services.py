import docker


class DockerAwsLogs:

    def __init__(
            self,
            args,
            logs_max_size: str = '10m',
            logs_max_files: int = 3,
            log_level: str = 'DEBUG'
        ):
        self.docker_image = args.docker_image
        self.bash_command = args.bash_command
        self.aws_cloudwatch_group = args.aws_cloudwatch_group
        self.aws_cloudwatch_stream = args.aws_cloudwatch_stream
        self.aws_access_key_id = args.aws_access_key_id
        self.aws_secret_access_key = args.aws_secret_access_key
        self.aws_region = args.aws_region

        self.logs_max_size = logs_max_size
        self.logs_max_files = logs_max_files
        self.log_level = log_level

        self.container_id = 0
        try:
            self.client = docker.from_env()
        except Exception as exc:
            print(f"Can't create docker client: {exc}")
            exit(1)

    def _parse_log_config(self):
        log_config = {
            "type": "awslogs",
            "config": {
                "awslogs-region": self.aws_region,
                "awslogs-group": self.aws_cloudwatch_group,
                "awslogs-stream": self.aws_cloudwatch_stream,
                "awslogs-create-group": "true",
            }
        }
        return log_config

    def _get_env(self):
        env = {
            "LOG_LEVEL": self.log_level,
            "AWS_ACCESS_KEY_ID": self.aws_access_key_id,
            "AWS_SECRET_ACCESS_KEY": self.aws_secret_access_key,
        }
        return env

    def run_container(self):
        env = self._get_env()
        log_config = self._parse_log_config()

        print(
            f"Starting {self.docker_image} container."\
            f"Cloudwatch group: {self.aws_cloudwatch_group}."\
            f"Cloudwatch stream: {self.aws_cloudwatch_stream}"
        )
        try:
            container = self.client.containers.run(
                image=self.docker_image,
                command=self.bash_command,
                detach=True,
                environment=env,
                log_config=log_config,
            )
        except Exception as exc:
            print(f"Can't run {self.docker_image} container: {exc}")
        else:
            self.container_id = container.id
