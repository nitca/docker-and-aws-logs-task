import argparse


def get_argparser() -> argparse.ArgumentParser.parse_args:
    """
    Получение объекта аргументов программы

    Returns:
        Объект для работы с аргументами
    """

    parser = argparse.ArgumentParser(
        description="Run docker with logs into AWS CloudWatch"
    )
    parser.add_argument(
        '--docker-image',
        default='python',
        required=False,
        type=str,
        help='Name of your docker image',
    )
    parser.add_argument(
        '--bash-command',
        type=str,
        required=True,
        help='Command for run into container',
    )
    parser.add_argument(
        '--aws-cloudwatch-group',
        type=str,
        required=True,
        help='Group name of logs',
    )
    parser.add_argument(
        '--aws-cloudwatch-stream',
        type=str,
        required=True,
        help='Names of threads for logs',
    )
    parser.add_argument(
        '--aws-access-key-id',
        type=str,
        required=True,
        help='Access ID for AWS',
    )
    parser.add_argument(
        '--aws-secret-access-key',
        type=str,
        required=True,
        help='Access key for AWS',
    )
    parser.add_argument(
        '--aws-region',
        type=str,
        default='eu-west-1',
        required=False,
        help='Region of AWS service',
    )
    parser.add_argument(
        '--log-level',
        type=str,
        default='DEBUG',
        choices=('DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'),
        help='Minimal log level'
    )

    return parser.parse_args()
